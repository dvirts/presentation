# Data Processing Packages 

### Python, SAS, R and MATLAB

#### September 26, 2016





# SAS

### Pros

- All of our programs and models currently reside in SAS 

- SAS is very good at analyzing larger than memory datasets with little input from the user 

- SAS documentation is fairly comprehensive 

- When the data is too larger for one machine we don't have to think about things like server management, clusters etc. We just need to include `rsubmit` and we're done

- All of us know SAS and have built up fairly comprehensive expertise in it

- Hard to implement Classes and other Computer Science concepts to prevent reusing  code

  ​

### Cons

- SAS is not free, we have to pay thousands of dollars in licensing fees

- Since SAS is a controlled environment, new developments in statistics come years after there are open source equivalents and usually cost extra. 

- Impossible to create Excel, Powerpoint etc. files within SAS. 

- While SAS has professional support, its community resources are somewhat lacking. 

- SAS graphics leave much to be desired. Creating and exporting plots with custom styles is difficult if not impossible in certain contexts 

- Manipulating data matrices is cumbersome and unintuitive

  ​

## Python

### Pros

- One of the easiest programming languages to learn and to read 

- Open Source with a friendly fast growing community

- Many large companies like Google, Facebook and Paypal use Python and support the language by funding open source projects or by employing the authors of many of the most popular packages

- Easy to implement Classes, Functions etc.  to keep code clean and readable

- Jupyter Notebooks make the data exploration loop and sharing your results very easy 

- New developments in statistics get implemented in Python very quickly 

- Scales to Big Data quite easily with libraries like Dask or with PostgreSQL as a backend database

  ​

## Cons

- While the main graphics library can make incredible graphs, it's a bit verbose and difficult to learn 

- Other graphics packages built to make this process easier exist, but there's a lack of a common language 

- Most Python developers use Linux and there are some Linux only packages. This is particularly true for heavy duty machine learning libraries that assume the user can easily compile C++ or Fortran libraries from source. 

  ​

## R

### Pros

- Open Source with a strong community actively developing packages 
- Most Academic Statisticians write their new algorithms in R, so often it's the first place that implementation occurs
- Support from companies like RevolutionR which makes it easy to troubleshoot problems 
- Has one of the most elegant graphics libraries in use right now - ggplot2 

### Cons

- R is a much harder language to learn than Python
- In particular most loops in R are handled by the apply function family which are not easy to learn
- Like Python Linux is the default language
- Also relies on memory to do data processing
- Much more package fragmentation on R than in Python and this lack of a unified syntax can make trying different methods daunting

## MATLAB

### Pros

- Easier to program custom functions than SAS 

- Large Standard Library

- Easier to learn than R or SAS 

  ​

### Cons

- Expensive 
- Have to wait for algorithms to get implemented in MATLAB
- Limited Portability - MATLAB Component Runtime changes every 6 months or so
- Some Third Party extensions to functionality but lack of an easy way to manage everything



# Comparisons

### Transforming Data 

Since a lot of what we do involves transforming data in various ways. Here are some examples using SAS, Python and R of common tasks. 



In this case we want to subtract the average total bill from each observed bill but with different means for smokers and non smokers and then display the results

#### SAS

```
proc summary data=tips missing nway;
    class smoker;
    var total_bill;
    output out=smoker_means mean(total_bill)=group_bill;
run;

proc sort data=tips;
    by smoker;
run;

data tips;
    merge tips(in=a) smoker_means(in=b);
    by smoker;
    adj_total_bill = total_bill - group_bill;
    if a and b;
run;

proc print data=tips(obs=5);
run;
```



#### Python (with Pandas)

```python
gb = tips.groupby('smoker')['total_bill']
tips['adj_total_bill'] = tips['total_bill'] - gb.transform('mean')
tips.head()
```



#### R (with dplyr)

```R
gb = groupby(tips, smoker)
gb %>% summarize(adj_total_bill = total-bill - mean(total_bill, na.rm=TRUE)) %>% slice(1:5)

```



### Graphs

If we wanted to graph a violin plot by the days of the week and smoking status, add a descriptive title and save the graph to file

#### SAS

In SAS this isn't possible at least not out of the box. The following code shows how to create violin plots for a dataset that's not the tips dataset 

```
data cholesterol;
  keep deathcause cholesterol; 
  set sashelp.heart;
  if deathcause ne '';
  run;

* first I'm just generating an index for your earlier dataset so that I don't have to create a dataset sorted by deathcause; 
proc datasets library=work;
modify cholesterol;
index create deathcause;
run;
quit;

ods html close;
ods listing;

* kde stands for kernel desnity estimation and the procedure does just that;
proc kde data = cholesterol;
by deathcause;
univar cholesterol (gridl=0 gridu=500) / NGRID=51 unistats percentiles plots=none /*(plots=(density HISTDENSITY)*/
     out = chol_dens (rename=(value=cholesterol) drop=var);
run;

data chol_den_2;
  set chol_dens;
    mirror=-density;
	zero=0;
run;

%let gpath='C:\';
%let dpi=200;

ods listing gpath=&gpath image_dpi=&dpi;

/*--Vertical Violin--*/
ods graphics / reset width=7in height=4in imagename='Interval_Violin_Vert';
title 'Violin Plot of Cholesterol Densities by Death Cause';
proc sgpanel data=chol_den_2 nocycleattrs;
  panelby deathcause / layout=columnlattice onepanel 
    novarname noborder colheaderpos=bottom;
  band y=cholesterol upper=density lower=mirror / fill outline;
  rowaxis label='Cholesterol' grid;
  colaxis display=none;
run;

```

Anyways the plots that this code produces look like this !["SAS Violins"](http://blogs.sas.com/content/graphicallyspeaking/files/2012/10/Violin_Band_Horz.png)

#### Python

```python
sns.violinplot(x="day", y="total_bill", hue="smoker", data=tips, split=True,
               inner="quart", palette={"No": "g", "Yes": "r"})
plt.xlabel("Day")
plt.ylabel("Total Bill")
plt.title("Average Tips Per Day by Smoking Status")
sns.despine()
plt.savefig("Python Violin Chart.jpg")
```

#### R

```R
p = ggplot(tips, aes(factor(day), total_bill)) +
  theme_minimal()
p = p +
  geom_violin(aes(fill = smoker), scale = "width", trim=FALSE) + 
  labs(title = "Average Tips per Day by Smoking Status", x ="Day", y="Total Bill")
ggsave(p, filename="R Violin Chart.jpg")
```

#### MATLAB

MATLAB has a user written Violin plot function. Here's an example of how it would be used in practice

```
figure(10)
subplot(1,2,1)
violin([rand_x rand_y],'facecolor',[[1 0 0];[0 0 1]],'medc','','mc','k')
legend off
box off
axis off
```

### Custom Functions

One function we heavily relied on during the model fitting process was a function that created the Weight of Evidence transformation based on a monotonic binning algorithm.  Here's that same algorithm in SAS, Python and R

#### SAS

The actual SAS Macro is a couple hundred lines of code longer than this but those codes are all used to write the results of the transformation to  a text file. The total number of lines of code to comment the function, compute the bins and output the bins in text form is 496

```
%macro num_woe(data = , y = , x = );
options nocenter nonumber nodate mprint mlogic symbolgen
        orientation = landscape ls = 150;

*** DEFAULT PARAMETERS ***;

%local maxbin minbad miniv bignum;

%let maxbin = 100;

%let minbad = 50;

%let miniv  = 0.03;

%let bignum = 1e300;

***********************************************************;
***         DO NOT CHANGE CODES BELOW THIS LINE         ***;
***********************************************************;
*** PARSING THE STRING OF NUMERIC PREDICTORS ***;
ods listing close;
ods output position = _pos1;
proc contents data = &data varnum;
run;

proc sql noprint;
  select
    upcase(variable) into :x2 separated by ' '
  from
    _pos1
  where
    compress(upcase(type), ' ') = 'NUM' and
    index("%upcase(%sysfunc(compbl(&x)))", compress(upcase(variable), ' ')) > 0;


  select
    count(variable) into :xcnt
  from
    _pos1
  where
    compress(upcase(type), ' ') = 'NUM' and
    index("%upcase(%sysfunc(compbl(&x)))", compress(upcase(variable), ' ')) > 0;
quit;

data _tmp1;
  retain &x2 &y;
  set &data;
  where &Y in (1, 0);
  keep &x2 &y;
run;

ods output position = _pos2;
proc contents data = _tmp1 varnum;
run;

*** LOOP THROUGH EACH PREDICTOR ***;
%do i = 1 %to &xcnt;
    
  proc sql noprint;
    select
      upcase(variable) into :var
    from
      _pos2
    where
      num= &i;

    select
      count(distinct &var) into :xflg
    from
      _tmp1
    where
      &var ~= .;
  quit;

  proc summary data = _tmp1 nway;
    output out  = _med(drop = _type_ _freq_)
    median(&var) = med nmiss(&var) = mis;
  run;
  
  proc sql;
    select
      med into :median
    from
      _med;

    select
      mis into :nmiss
    from
      _med;

    select 
      case when count(&y) = sum(&y) then 1 else 0 end into :mis_flg1
    from
      _tmp1
    where
      &var = .;

    select
      case when sum(&y) = 0 then 1 else 0 end into :mis_flg2
    from
      _tmp1
    where
      &var = .;
  quit;

  %let nbin = %sysfunc(min(&maxbin, &xflg));

  *** CHECK IF THE NUMBER OF DISTINCT VALUES > 1 ***;
  %if &xflg > 1 %then %do;

    *** IMPUTE MISS VALUE WHEN WOE CANNOT BE CALCULATED ***;
    %if &mis_flg1 = 1 | &mis_flg2 = 1 %then %do;
      data _null_;
        file impfile mod;
        put " ";
        put @3 "*** MEDIAN IMPUTATION OF %TRIM(%UPCASE(&VAR)) (NMISS = %trim(&nmiss)) ***;";
        put @3 "IF %TRIM(%UPCASE(&VAR)) = . THEN %TRIM(%UPCASE(&VAR)) = &MEDIAN;";
      run;

      data _tmp1;
        set _tmp1;
        if &var = . then &var = &median;
      run; 
    %end;      
      
    *** LOOP THROUGH THE NUMBER OF BINS ***;
    %do j = &nbin %to 2 %by -1;
      proc rank data = _tmp1 groups = &j out = _tmp2(keep = &y &var rank);
        var &var;
        ranks rank;
      run;

      proc summary data = _tmp2 nway missing;
        class rank;
        output out = _tmp3(drop = _type_ rename = (_freq_ = freq))
        sum(&y)   = bad    mean(&y)  = bad_rate
        min(&var) = minx   max(&var) = maxx;
      run;

      *** CREATE FLAGS FOR MULTIPLE CRITERION ***;
      proc sql noprint;
        select
          case when min(bad) >= &minbad then 1 else 0 end into :badflg
        from
          _tmp3;

        select
          case when min(bad_rate) > 0 then 1 else 0 end into :minflg
        from
          _tmp3;

        select
          case when max(bad_rate) < 1 then 1 else 0 end into :maxflg
        from
          _tmp3;              
      quit;

      *** CHECK IF SPEARMAN CORRELATION = 1 ***;
      %if &badflg = 1 & &minflg = 1 & &maxflg = 1 %then %do;
        ods output spearmancorr = _corr(rename = (minx = cor));
        proc corr data = _tmp3 spearman;
          var minx;
          with bad_rate;
        run;

        proc sql noprint;
          select
            case when abs(cor) = 1 then 1 else 0 end into :cor
          from
            _corr;
        quit;

        *** IF SPEARMAN CORR = 1 THEN BREAK THE LOOP ***;
        %if &cor = 1 %then %goto loopout;
      %end;
      %else %if &nbin = 2 %then %goto exit;
    %end;

*********************************************************;
*           END OF NUM_WOE MACRO                        *;
*********************************************************;
%mend num_woe;

```



#### Python

The following function is the same algorithm implemented by the same programmer in Python 2.7. (This includes the code that outputs the results in text form)

```python
def mono_bin(Y, X, n = 20):
  # fill missings with median
  X2 = X.fillna(np.median(X))
  r = 0
  while np.abs(r) < 1:
    d1 = pd.DataFrame({"X": X2, "Y": Y, "Bucket": pd.qcut(X2, n)})
    d2 = d1.groupby('Bucket', as_index = True)
    r, p = stats.spearmanr(d2.mean().X, d2.mean().Y)
    n = n - 1
  d3 = pd.DataFrame(d2.min().X, columns = ['min_' + X.name])
  d3['max_' + X.name] = d2.max().X
  d3[Y.name] = d2.sum().Y
  d3['total'] = d2.count().Y
  d3[Y.name + '_rate'] = d2.mean().Y
  d4 = (d3.sort_index(by = 'min_' + X.name)).reset_index(drop = True)
  print "=" * 60
  print d4
```



#### R

The same programmer also implemented a version in R although it doesn't report the max and min values for each bin like the SAS and R functions do.

```R
bin <- function(x, y){
  n <- min(50, length(unique(x)))
  repeat {
    n   <- n - 1
    d1  <- data.frame(x, y, bin = cut2(x, g = n)) 
    d2  <- aggregate(d1[-3], d1[3], mean)
    cor <- cor(d2[-1], method = "spearman")
    if(abs(cor[1, 2]) == 1) break
  }
  d2[2] <- NULL
  colnames(d2) <- c('LEVEL', 'RATE')
  head <- paste(toupper(substitute(y)), " RATE by ", toupper(substitute(x)), sep = '')
  cat("+-", rep("-", nchar(head)), "-+\n", sep = '')
  cat("| ", head, ' |\n', sep = '')
  cat("+-", rep("-", nchar(head)), "-+\n", sep = '')
  print(d2)
  cat("\n")
}
```

